<?php
session_start();
require './src/mysqlConnect.php';
// require './src/update_slots.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=yes, , initial-scale=1">
    <title>Car Parking Portal</title>
		<link rel="icon" href="../../assets/img/car.png">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="custom.css">

<style>
/* .modal-fullscreen size: using Bootstrap media query breakpoints */
.modal-fullscreen .modal-dialog {
  margin: 0;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
}
@media (min-width: 768px) {
  .modal-fullscreen .modal-dialog {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .modal-fullscreen .modal-dialog {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .modal-fullscreen .modal-dialog {
     width: 1170px;
  }
}

/* .modal-transparent */
#regForm label{
color: #000 !important; /* makes the text-black */
}
</style>

<script type="text/javascript">
function checkPass(){
    var pass1 = document.getElementById('password');
    var pass2 = document.getElementById('cpassword');

   // confirmation message object
    var message = document.getElementById('confirmMessage');

    var goodColor = "#66cc66";
    var badColor = "#ff6666";

    if(pass1.value == pass2.value){
        pass2.style.backgroundColor = goodColor;

        $('#regBtn').prop('disabled', false);
        $('#regOwner').prop('disabled', false);
    }
    else{
        $('#regBtn').prop('disabled', true);
        $('#regOwner').prop('disabled', true);
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords isn't matching!"
    }
}
</script>
</head>

  <body>
    <div class="overlay">
    <div class="row">
      <div class="container">
         <div class="col-md-4"></div>
         <div class="col-md-4">
               <div class="page-header">
                 <center><h1 class="colors">Car Parking Portal</h1></center>
               </div>
         </div>
         <div class="col-md-4"></div>
      </div>
    </div>
    <!-- Modal -->
  <div class="modal modal-fullscreen fade modal-transparent" id="myModal"  role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><h4 class="modal-title" style="color:#225556;">Register an account</h4></center>
        </div>
        <div class="modal-body">
          <form  id="regForm" action="./src/register.php" method="POST" enctype="multipart/form-data">

    <div class="form-group">
      <label for="name" >Full Name</label>
      <input type="text" id="name"  name="name"  placeholder="John Doe" class="text ui-widget-content ui-corner-all" required="true">
  </div>

  <div class="form-group">
      <label for="email">Email</label>
      <input type="email" id="email" name="email" placeholder="john@example.com" class="text ui-widget-content ui-corner-all"  onchange="email_validate(this.value);" required="true">
  </div>

   <div class="form-group">
      <label for="password">Password</label>
      <input type="password" id="password" name="password" placeholder="Password" class="text ui-widget-content ui-corner-all" required="true">
  </div>

   <div class="form-group">
      <label for="password">Confirm Password</label>
      <input type="password" id="cpassword" name="cpassword" placeholder="Password" class="text ui-widget-content ui-corner-all"  onkeyup="checkPass(); return false;" required="true">
		</div>
		<div class="form-group">
        <label for="license"><b>License</b></label>
        <input type="text" placeholder="License No." name="license" required="true">
    </div>

    <div class="form-group">
      <label for="addr"><b>Address</b></label>
      <input type="text" placeholder="Address" name="addr" required="true">
      <label for="pin"><b>Pin</b></label>
      <input type="text" placeholder="Pin Code" name="pin" required="true">
    </div>

    <div class="form-group">
      <label for="mobile"><b>Mobile no: </b></label>
      <input type="mobile_no" placeholder="Mobile No" name="mobile" required="true">
    </div>

    <p>By creating an account you're agreeing with our <a style="color:dodgerblue" href="./privacy.txt">Terms of Service & Privacy Policy</a>.</p>

    <button type="submit" id="regBtn" class="form-control" name="register">Register</button>
    </div>
    </form>
    </div>

    </div>
  </div>


    <div class="row">
      <div class="container">
         <div class="col-md-4"></div>
         <div class="col-md-4">

           <form enctype="multipart/form" action="index.php" method="POST" id="">
                   <div class="page-header">
                     <center><h3 class="colors">Login</h3></center>
                   </div>

            <label for=""></label>
             <input type="text" name="email" id="" placeholder="email" class="email">

             <label for=""></label>
             <input type="password" name="password" id="" placeholder="password" class="pass">

             	<!-- <button type="submit" name="login">Sign in</button> -->
							 <button name='login' type="submit"><i class="fa fa-lock" aria-hidden="true"></i>  Sign In</button>
							<label for=""></label>
							<button type="submit" name="admin"><i class="fa fa-lock" aria-hidden="true"></i>  Admin</button>
							<label for=""></label>
              <button type="button" data-toggle="modal" data-target="#myModal">Register</button>
           </form>

         </div>
         <div class="col-md-4"> </div>
    </div>
  </div>
</div>
<div style="max-width:500px; margin: 3em auto 0 auto; text-align: center; font-size: 0.85em">
<a rel="license" href="https://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0;" src="https://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/" style="color: #000000">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>
<br>
<span>(c) 2018-2019 Software Engineering Project </span>
<p><a rel="souce-code" href="https://gitlab.com/finn02" style="color: #000000">Get Source Code</a> v1.0.7 </p>
</div>

<?php
if(isset($_POST['login'])){
$password=mysqli_real_escape_string($con,$_POST['password']);
$email=mysqli_real_escape_string($con,$_POST['email']);

$sel="select * from users where email='$email'";
$result=mysqli_query($con,$sel);
if(mysqli_num_rows($result) > 0){

  while($row = mysqli_fetch_array($result)){

    if(password_verify($password, $row["password"])){
      $_SESSION['driver_email']=$email;
      echo"<script>window.open('./src/book/book.php','_self')</script>";
    }
    else{
       echo"<script>alert('Wrong credentials,try again!')</script>";
       echo"<script>window.open('index.php','_self')</script>";
       exit();
    }
  }
}
else{
    echo"<script>alert('Wrong credentials, try again!')</script>";
    echo"<script>window.open('index.php','_self')</script>";
    exit();
	}
}
else if(isset($_POST['admin'])){

	echo"<script>window.open('./src/admin/admin_login.php','_self')</script>";
	exit();
 }
?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>

