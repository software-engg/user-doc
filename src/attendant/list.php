<?php session_start();
require '../mysqlConnect.php';
require '../update_slots.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Car parking</title>
    <link rel="icon" href="../../assets/img/admin.jpg">

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header black-bg">
            <!--logo start-->
            <a href="../../index.php" class="logo"><b>Home</b></a>
            <!--logo end-->

        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">

                    <p class="centered"><a href="#"><img src="../../assets/img/admin.png" class="img-circle" width="60"></a></p>
                    <h5 class="centered"> <?php echo $_SESSION['email']; ?></h5>

                  <li class="mt">
                      <a href="../admin/admin.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
				<div class="row">

	                  <div class="col-md-12 mt">
	                  	<div class="content-panel">
	                          <table class="table table-bordered">

                                      <tr><h2>View All Parking Attendants </h2></tr>
                                      <tr>
                                      <th>ID</th>
                                      <th>Username </th>
                                      <th>Name </th>
                                      <th>Contact no.</th>
                                      <th>Spot ID</th>
                                      <th>Delete </th>
                                      </tr>
                <?php
                $sel="select * from attendant";
                $run=mysqli_query($con,$sel);
                $i=0;
                while($row=mysqli_fetch_array($run)){
								$id_attendant=$row['id_attendant'];
								$user=$row['username'];
                $Fname=$row['Fname'];
                $Lname=$row['Lname'];
                $mobile_no=$row['mobile_no'];
                $location=$row['location'];
                $i++;

                ?>
                <tr>
                <td><?php echo $i; ?></td>
								<td><?php echo $user; ?></td>
                <td><?php echo $Fname .' '. $Lname; ?></td>
                <td><?php echo $mobile_no; ?></td>
                <td><?php echo $location; ?></td>
                <td><a href="list.php?delete=<?php echo $id_attendant; ?>">Delete</a></td>
                </tr>
                <?php }?>
                </table>
                <?php
                if(isset($_GET['delete'])){
                  $delete_id=$_GET['delete'];
                  $delete="DELETE FROM `attendant` WHERE `attendant`.`id_attendant` ='$delete_id'";
                  $run_delete=mysqli_query($con,$delete);
                  if($run_delete)
                  {
                    echo "<script>alert('Attendant deleted successfully')</script>";
                    echo "<script>window.open('list.php','_self')</script>";
                  }
                }
                ?>
	                  	  </div>
	                  </div>
				</div>

		</section><!--wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../../assets/js/common-scripts.js"></script>
    <!--script for this page-->

  <script>
      //custom select box
      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
